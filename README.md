# 文阳记账

#### 介绍
文阳记账：使用UniApp开发的一款单机版的记账工具，适用餐饮行业，可打包H5、Android、IOS、小程序平台，适配平板端及手机端。

#### 软件架构

本系统使用UniApp开发，本系统为单机版，可直接运行。

#### 安装教程

请导入至HbuilderX进行调试或者运行