export default {
	getDate(strDate) {
		var now = new Date();
		var year = now.getFullYear();
		var month = now.getMonth() + 1;
		var day = now.getDate();
		var hh = now.getHours();
		var mm = now.getMinutes();
		var ss = now.getSeconds();
		var clock = year + "-";
		if (month < 10)
			clock += "0";
		clock += month + "-";
		if (day < 10)
			clock += "0";
		clock += day + " ";
		if (hh < 10)
			clock += "0";
		clock += hh + ":";
		if (mm < 10) clock += '0';
		clock += mm + ":";
		if (ss < 10) clock += '0';
		clock += ss;
		return clock;
	},
	getStorage(key, defaultValue) {
		let value = uni.getStorageSync(key)
		if (value === "") {
			return defaultValue
		}
		return value
	}
}
